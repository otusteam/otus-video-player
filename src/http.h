
#pragma once

#include <glib.h>

extern const gchar* useragent;

gchar* do_request(const gchar* URL, gsize* page_size);
